#!/bin/bash
ACTION=$1
#Install mysql in Debian
installAtDebian(){
apt update
wget -c https://repo.mysql.com//mysql-apt-config_0.8.13-1_all.deb
dpkg -i mysql-apt-config_0.8.13-1_all.deb
apt-get install mysql-server mysql-client -y
mysql_secure_installation

}
#Install mysql in Redhat
installAtRedhat(){
yum remove mariadb-server -y
yum install wget -y
wget https://dev.mysql.com/get/mysql57-community-release-el7-8.noarch.rpm
rpm -ivh mysql57-community-release-el7-8.noarch.rpm
yum install mysql-server -y
systemctl start mysqld
systemctl enable mysqld
mysql_secure_installation

}
#Remove mysql in Debian
removeAtDebian(){
apt remove --purge mysql-server mysql-client -y
apt purge mysql-server mysql-client -y
apt autoremove -y
apt autoclean -y
apt remove dbconfig-mysql -y

}
#Remove mysql in Redhat
removeAtRedhat(){
yum remove mysql -y
yum remove mysql-server mysql-client -y
yum clean all
}
#Conditions
case $ACTION in
       install)
                if [ "$(. /etc/os-release; echo $NAME)" = "Ubuntu" ]; then
                installAtDebian
                elif [ "$(. /etc/os-release; echo $NAME)" = "Red Hat Enterprise Linux" ]; then
                installAtRedhat
                fi
                ;;
        remove)
                if [ "$(. /etc/os-release; echo $NAME)" = "Ubuntu" ]; then
                removeAtDebian
                elif [ "$(. /etc/os-release; echo $NAME)" = "Red Hat Enterprise Linux" ]; then
                removeAtRedhat
                fi
                ;;
             *)
                echo "wrong input"
                ;;
esac
